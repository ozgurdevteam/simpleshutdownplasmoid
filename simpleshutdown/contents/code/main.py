# -*- coding: utf-8 -*-
__author__ = 'ozgur'
__creation_date__ = '12/23/14' '12:22 AM'

from PyKDE4 import plasmascript

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QGraphicsLinearLayout, QIcon
from PyKDE4.plasma import Plasma
import commands


class HelloWorldApplet(plasmascript.Applet):
    def __init__(self, parent, args=None):
        plasmascript.Applet.__init__(self, parent)

    def init(self):
        self.setHasConfigurationInterface(False)
        self.setAspectRatioMode(Plasma.Square)
        self.theme = Plasma.Svg(self)
        self.theme.setImagePath("widgets/background")
        self.setBackgroundHints(Plasma.Applet.DefaultBackground)
        self.layout = QGraphicsLinearLayout(Qt.Vertical, self.applet)
        self.btn_shutdown = Plasma.IconWidget()
        icon = QIcon.fromTheme("system-shutdown")
        self.btn_shutdown.setIcon(icon)
        self.layout.addItem(self.btn_shutdown)
        self.applet.setLayout(self.layout)
        self.resize(130, 50)
        self.btn_shutdown.clicked.connect(self.shut_me_down)

    def shut_me_down(self):
        output = commands.getstatusoutput("qdbus org.kde.ksmserver /KSMServer org.kde.KSMServerInterface.logout -1 -1 -1")


def CreateApplet(parent):
    return HelloWorldApplet(parent)